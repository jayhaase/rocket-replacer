<?php

use jayhaase\rocketreplacer\RocketReplacer;
use jayhaase\rocketreplacer\Tree;

use PHPUnit\Framework\TestCase;


class RocketReplaceTest extends TestCase
{
    private $tree         = null;
    private $akronel      = null;
    private $skyRope      = null;
    private $ropeThread2  = null;
    private $akronelsRing = null;


    public function setUp(): void
    {
        $this->tree         = new Tree();
        $this->akronel      = '[AKRONEL]';
        $this->skyRope      = '[SKY ROPE]';
        $this->ropeThread2  = '[ROPE THREAD 2]';
        $this->akronelsRing = '[AKRONELS RING]';
        $this->cowTener     = '[COW TENER]';
        $this->theBoat      = '[THE BOAT]';
        $this->theMove      = '[THE MOVE]';
        $this->well         = '[WELL]';

        $this->tree->addPaths([
            'Akronel'        => $this->akronel,
            'Sky Rope'       => $this->skyRope,
            'Rope Thread 2'  => $this->ropeThread2,
            "Akronel's Ring" => $this->akronelsRing,
            'Cow Tener'      => $this->cowTener,
            'The Boat'       => $this->cowTener,
            'Move - Drink'   => $this->theMove,
            'Well'           => $this->well,
        ]);
    }


    public function teadDown(): void
    {
        $this->tree         = null;
        $this->akronel      = null;
        $this->skyRope      = null;
        $this->ropeThread2  = null;
        $this->akronelsRing = null;
        $this->cowTener     = null;
        $this->theBoat      = null;
        $this->theMove      = null;
        $this->well         = null;
    }


    public function doReplaceTest($rawText, $resultText): void
    {
        $replacer = new RocketReplacer($this->tree);
        $result   = $replacer->replaceTokens($rawText);

        $this->assertEquals($resultText, $result);
    }


    public function testNoMatches(): void
    {
        $this->doReplaceTest(
            'This has no pAkronel items in it.',
            'This has no pAkronel items in it.'
        );
    }


    public function testOneAlphaWordMatch(): void
    {
        $this->doReplaceTest(
            'Akronel',
            $this->akronel
        );
    }


    public function testIgnoreTerminatorAtEndOfString(): void
    {
        $this->doReplaceTest(
            'Akronel@',
            $this->akronel
        );
    }


    public function testMatchAtBeginningOfString(): void
    {
        $this->doReplaceTest(
            'Akronel was here',
            $this->akronel . ' was here'
        );
    }


    public function testMatchWithManySpaces(): void
    {
        $this->doReplaceTest(
            'I am    Akronel the witch.',
            'I am    ' . $this->akronel . ' the witch.'
        );
    }

    public function testMatchWithQuestionMark(): void
    {
        $this->doReplaceTest(
            'Am I Akronel?',
            'Am I ' . $this->akronel . '?'
        );
    }

    public function testMatchWitParens(): void
    {
        $this->doReplaceTest(
            'Am I (Akronel)?',
            'Am I (' . $this->akronel . ')?'
        );
    }

    public function testMatchWithTwoWords(): void
    {
        $this->doReplaceTest(
            'This is a sky rope for the win.',
            'This is a ' . $this->skyRope . ' for the win.'
        );
    }


    public function testMatchWithThreeWords(): void
    {
        $this->doReplaceTest(
            'This is a rope thread 2 because of reasons.',
            'This is a ' . $this->ropeThread2 . ' because of reasons.'
        );
    }


    public function testMatchLongerTerm(): void
    {
        $this->doReplaceTest(
            "That is Akronel's ring is mine.",
            'That is ' . $this->akronelsRing . ' is mine.'
        );
    }


    public function testWithinAMarkdownLinkShouldNotBeReplaced(): void
    {
        $this->doReplaceTest(
            "[flexible, whip-like blade](https://en.wikipedia.org/wiki/Akronel)",
            "[flexible, whip-like blade](https://en.wikipedia.org/wiki/Akronel)"
        );
    }


    /**
     * The word "the" proceeds cow tener, and the word "the" is part of another term.
     */
    public function testForCowTener(): void
    {
        $this->doReplaceTest(
            "center of the Cow tener.",
            "center of the [COW TENER]."
        );
    }


    /**
     * Have a term embbedded in a word (well) -- this should not be replaced.
     */
    public function testForDashesAllowed(): void
    {
        $this->doReplaceTest(
            "You are doing the Move - Drink from the glass.",
            "You are doing the [THE MOVE] from the glass."
        );
    }

    /**
     * Have a term embbedded in a word (well) -- this should not be replaced.
     */
    public function testDoNotReplaceEmbededWords(): void
    {
        $this->doReplaceTest(
            "Let's go welling.",
            "Let's go welling."
        );
    }

}
