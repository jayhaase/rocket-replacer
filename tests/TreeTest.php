<?php

use jayhaase\rocketreplacer\Tree;
use jayhaase\rocketreplacer\TreeNode;

use PHPUnit\Framework\TestCase;


class TreeTest extends TestCase
{
    public function testConstructedTreeHasNode(): void
    {
        $tree = new Tree();

        $this->assertInstanceOf(
            TreeNode::class,
            $tree->getRoot()
        );
    }


    public function testGetPayloadFromTreeWithOnePath(): void
    {
        $tree        = new Tree();
        $replacement = 'Yellow Bird';

        $tree->addPath('xyzzy', $replacement);
        $result = $tree->getReplacement('xyzzy');

        $this->assertEquals(
            $replacement,
            $result
        );
    }


    public function testRequestMissingPathFromTreeWithOnePath(): void
    {
        $tree        = new Tree();
        $replacement = 'Yellow Bird';

        $tree->addPath('xyzzy', $replacement);
        $result = $tree->getReplacement('xyzzyz');

        $this->assertNull($result);
    }


    public function testGetFirstPathFromMultiplePaths(): void
    {
        $tree         = new Tree();
        $replacement1 = 'Frog';
        $replacement2 = 'Kuume';
        $replacement3 = 'Katrina';

        $tree->addPath('xy', $replacement1);
        $tree->addPath('xyzz', $replacement2);
        $tree->addPath('xyzzy', $replacement3);

        $result = $tree->getReplacement('xy');

        $this->assertEquals(
            $replacement1,
            $result
        );
    }


    public function testGetSecondPathFromMultiplePaths(): void
    {
        $tree     = new Tree();
        $replacement1 = 'Frog';
        $replacement2 = 'Kuume';
        $replacement3 = 'Katrina';

        $tree->addPaths([
            'xy'    => $replacement1,
            'xyzz'  => $replacement2,
            'xyzzy' => $replacement3,
        ]);

        $result = $tree->getReplacement('xyzz');

        $this->assertEquals(
            $replacement2,
            $result
        );
    }


    public function testGetLastPathFromMultiplePaths(): void
    {
        $tree     = new Tree();
        $replacement1 = 'Frog';
        $replacement2 = 'Kuume';
        $replacement3 = 'Katrina';

        $tree->addPath('xyzzy', $replacement3);
        $tree->addPath('xy', $replacement1);
        $tree->addPath('xyzz', $replacement2);

        $result = $tree->getReplacement('xyzzy');

        $this->assertEquals(
            $replacement3,
            $result
        );
    }

    public function testGetOneCharacterDivergentPath(): void
    {
        $tree     = new Tree();
        $replacement1 = 'Frog';
        $replacement2 = 'Kuume';
        $replacement3 = 'Katrina';
        $replacement4 = 'Mouse House';

        $tree->addPath('xy', $replacement1);
        $tree->addPath('xyzzy', $replacement3);
        $tree->addPath('a', $replacement4);
        $tree->addPath('xyzz', $replacement2);

        $result = $tree->getReplacement('a');

        $this->assertSame(
            $replacement4,
            $result
        );
    }
}
