<?php

use jayhaase\rocketreplacer\TreeNode;

use PHPUnit\Framework\TestCase;


class TreeNodeTest extends TestCase
{
    public function testConstructedNodeHasNoChildren(): void
    {
        $node = new TreeNode('a');

        $this->assertFalse($node->hasChildren());
    }

    public function testConstructedNodeCharacterCanBeRetrieved(): void
    {
        $node = new TreeNode('z');

        $this->assertEquals(
            'z',
            $node->getCharacter()
        );
    }

    public function testSetCharacterCanBeRetrieved(): void
    {
        $node = new TreeNode('a');
        $node->setCharacter('z');

        $this->assertEquals(
            'z',
            $node->getCharacter()
        );
    }
}
