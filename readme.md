#Overview
Rocker Replacer provides a fast way to search through a string and replace tokens within the string. The tokens to be replaced are stored in a special tree structure that can be traversed character by character. 

The replacer will use the longest token that it can find if more than one token match a part of a string.

#Running Tests
```
composer install
./vendor/bin/phpunit --bootstrap vendor/autoload.php --testdox tests
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests
```