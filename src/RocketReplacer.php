<?php

namespace jayhaase\rocketreplacer;


/**
 * This class uses the Tree of replacements to parse through a string finding and replacing
 * any occurrence of strings stored in the Tree. The Tree is organized based on word paths and
 * can be searched very fast.
*/
class RocketReplacer {

    private $tree;
    private $input;
    private $inputPos;
    private $inputLen;
    private $output;

    /**
     * Character types.
     */
    const TOKEN_CHAR     = 1;  // a character that is used in the word(s) of a token.
    const SEPERATOR_CHAR = 2;  // a character that is used to separate words of a token.
    const IGNORE_CHAR    = 3;  // a character that should not be copied to the output.


    public function __construct(Tree $tree)
    {
        $this->tree = $tree;
        $this->resetParser();
    }


    private function resetParser(string $string = null): void
    {
        $this->input    = $string;
        $this->output   = null;
        $this->inputPos = 0;
        $this->inputLen = strlen($string);
    }


    /**
     * Look in the string and make replacements based on the contents of the parser's Tree.
     */
    public function replaceTokens(string $string): string
    {
        $this->resetParser($string);

        while (! $this->atEndOfInput()) {
            $this->moveToTokenStart();
            $this->evaluateToken();
        }

        return $this->output;
    }


    private function moveToTokenStart(): void
    {
        $prevCharHash = $this->evaulatePrevoiusCharacter();

        while (! $this->atEndOfInput()) {

            $charHash = $this->evaulateCurrentCharacter();

            if (($charHash['type'] === self::TOKEN_CHAR) && ($prevCharHash['type'] !== self::TOKEN_CHAR)) {
                // We found the start of a token.
                return;
            }

            // We have not found the start of a token "output" the current character.
            $this->addCharHashToOutput($charHash);
            $this->inputPos++;
            $prevCharHash = $charHash;
        }
    }


    private function evaluateToken(): void
    {
        if ($this->atEndOfInput()) {
            return;
        }

        $currentNode    = $this->tree->getRoot();
        $token          = null;
        $replacement    = null;
        $firstSeparator = null;

        while (! $this->atEndOfInput()) {

            // Get the current character and see if we have a match in the tree.
            $charHash  = $this->evaulateCurrentCharacter();
            $token    .= $charHash['char'];

            // If we pass a seperator character, record it so we can jump back
            // to it if we don't find a match.
            if (($charHash['type'] === self::SEPERATOR_CHAR) && !$firstSeparator) {
                $firstSeparator = [
                    'pos'   => $this->inputPos,
                    'token' => $token,
                ];
            }

            $childNode = $currentNode->getChild($charHash['char']);

            if (!$childNode) {
                // There was no matching path in the Tree for this token -- where done with this token.

                if ($charHash['type'] == self::TOKEN_CHAR) {
                    // We are in the middle of a token so clear out the replacement -- we
                    // don't replace parts of tokens.
                    $replacement = null;
                }

                break;
            }

            // We have found a matching path in the Tree for our current token.
            $currentNode = $childNode;

            if ($childNode->getReplacement()) {
                // The matching path has a replacement. Maybe there is a longer match so we
                // need to keep looking. In cases there is not a longer match,
                // save this one away along with its ending position.
                $replacement = array(
                    'string'   => $childNode->getReplacement(),
                    'inputPos' => $this->inputPos,
                );
            }

            $this->inputPos++;
        }

        // We finished trying to find a token. Let's see if we found a replacement in the Tree.
        if ($replacement) {
            $token = $replacement['string'];

            // Reset the input position to the end of the matched token, in case
            // it was found before the end of the loop.
            $this->inputPos = $replacement['inputPos'];
        }
        else if ($firstSeparator) {
            // There was no replacement, and while parsing we passed a seperator
            // character. Let's jump back to it and look for new tokens.
            $token          = $firstSeparator['token'];
            $this->inputPos = $firstSeparator['pos'];
        }

        $this->output  .= $token;
        $this->inputPos++;
    }


    private function atEndOfInput(): bool
    {
        if ($this->inputPos < $this->inputLen) {
            return false;
        }

        return true;
    }


    private function addCharHashToOutput(array $charHash): void
    {
        if (!$charHash) {
            return;
        }

        if ($charHash['type'] === self::IGNORE_CHAR) {
            return;
        }

        $this->output .= $charHash['char'];
    }


    private function evaulatePrevoiusCharacter(): array
    {
        if ($this->inputPos == 0) {
            return array(
                'char' => null,
                'type' => self::SEPERATOR_CHAR,
            );
        }

        $character = $this->input[$this->inputPos - 1];
        return $this->evaulateCharacter($character);
    }


    private function evaulateCurrentCharacter(): array
    {
        $character = $this->input[$this->inputPos];
        return $this->evaulateCharacter($character);
    }


    /**
     * We consider / a part of a token so that we don't match words in strings.
     */
    private function evaulateCharacter($character): array
    {
        if (($character >= 'a' && $character <= 'z') ||
            ($character >= 'A' && $character <= 'Z') ||
            ($character >= 'A' && $character <= 'Z') ||
            ($character == '/')) {

            return array(
                'char' => $character,
                'type' => self::TOKEN_CHAR,
            );
        }

        if ($character === '@') {
            return array(
                'char' => $character,
                'type' => self::IGNORE_CHAR,
            );
        }

        return array(
            'char' => $character,
            'type' => self::SEPERATOR_CHAR,
        );
    }
}