<?php

namespace jayhaase\rocketreplacer;


/**
 * This class creates a tree where each node is represented by a charactger. Each path through the tree
 * spells out a word or phrase. This tree is used to quickly detect known words or phrases by
 * traversing the tree one letter at a time.
 */
class Tree
{
    private $root = null;


    public function __construct()
    {
        $this->root = new TreeNode(null);
    }


    public function getRoot(): TreeNode
    {
        return $this->root;
    }


    /**
     * This function reads a hash. Building paths in the tree based on the hash key
     * The hash value is stored in the last node of the path.
     */
    public function addPaths(array $hash): void
    {
        foreach ($hash as $key => $value) {
            $this->addPath($key, $value);
        }
    }


    public function addPath(string $key, string $replacement): void
    {
        $characters  = str_split($key);
        $currentNode = $this->root;

        foreach ($characters as $character) {
            $currentNode = $currentNode->addChild($character);
        }

        $currentNode->setReplacement($replacement);
    }

    /**
     * Use the proviced key to traverse the tree and return the replacement at the end of the path.
     * Null is returned if the key does not resolve to a replacement.
     */
    public function getReplacement(string $key): ?string
    {
        $characters  = str_split($key);
        $currentNode = $this->root;

        foreach ($characters as $character) {

            $currentNode = $currentNode->getChild($character);

            if (!$currentNode) {
                return null;
            }
        }

        $replacement = $currentNode->getreplacement();
        return $replacement;
    }
}