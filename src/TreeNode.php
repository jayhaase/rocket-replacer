<?php

namespace jayhaase\rocketreplacer;

/**
 * This class is one node of a tree. Each node of the tree can have zero, one, or more children.
 * The ID of a node is a lower case character. Each node can hold a reference to a string.
 */
class TreeNode
{
    private $children    = null;
    private $replacement = null;
    private $char        = null;


    public function __construct($char = null)
    {
        $this->setCharacter($char);
    }


    public function hasChildren(): bool
    {
        if (!$this->children) {
            return false;
        }

        if (count($this->children) > 0) {
            return true;
        }

        return false;
    }


    public function getChild($char): ?TreeNode
    {
        if (! $this->children) {
            return null;
        }

        $char = strtolower($char);

        if (array_key_exists($char, $this->children)) {

            $child = $this->children[$char];
            return $child;
        }

        return null;
    }


    public function addChild($char): TreeNode
    {
        $child = $this->getChild($char);

        if ($child) {
            return $child;
        }

        $child = new TreeNode($char);
        $char  = strtolower($char);

        $this->children[$char] = $child;

        return $child;
    }


    public function getCharacter(): string
    {
        return $this->char;
    }


    public function setCharacter($char): void
    {
        $this->char = $char;
    }


    public function getReplacement(): ?string
    {
        return $this->replacement;
    }


    public function setReplacement(string $replacement): void
    {
        $this->replacement = $replacement;
    }
}